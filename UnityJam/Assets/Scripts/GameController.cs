﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public enum state { Playing, Paused, GameEnd}
    public state GameState;
    public ModeMenu menu;
    public Hud hud;
    public Player[] player;

    public int p1Score, p2Score;
    public float gameLength;

    void Start()
    {
        menu = GetComponent<ModeMenu>();
        hud = GetComponent<Hud>();
        GameState = state.Playing;
    }

    public void updateScore()
    {
        //do stuff
    }

    void Update()
    {
        gameLength -= Time.deltaTime;
        gameTime();

        if (Input.GetKeyDown(KeyCode.P))
        {
            menu.pauseGame(menu.pauseMenu);
            GameState = state.Paused;
            //pause game code
        }
    }

    public void gameTime()
    {
        int minutes = Mathf.FloorToInt(gameLength / 60f);
        int seconds = Mathf.FloorToInt(gameLength - minutes * 60);
        string gameTimeFormat = string.Format("{0:0}:{1:00}", minutes, seconds);
        hud.countdownText.text = gameTimeFormat;
    }

}
