﻿using UnityEngine;

public class Player : MonoBehaviour {
    [HideInInspector]
    public bool facingRight = true;
    [HideInInspector]
    public bool jump = false;
    
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    
    private bool isGrounded = false;
    private Transform groundCheck;
    

	// Use this for initialization
	void Awake () {
	   groundCheck = transform.Find("GroundCheck");
	}
	
	// Update is called once per frame
	void Update () {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
	    isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
	                 
        if (Input.GetButtonDown("Jump"))
            jump = true;                          
	}
    
    void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb.velocity.x < maxSpeed)
            rb.AddForce(Vector2.right * moveHorizontal * moveForce);
            
        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
                rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);            
            
        if (moveHorizontal > 0 && !facingRight)
            Flip();
        else if (moveHorizontal < 0 && facingRight)
            Flip();
        
        if (jump)
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }
    
    void Flip()
    {
        facingRight = !facingRight;
        
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;            
    }
}
