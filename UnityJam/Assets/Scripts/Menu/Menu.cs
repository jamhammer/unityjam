﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject currentMenu;

    public void loadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void menuSwitch(GameObject obj)
    {
        currentMenu.SetActive(false);
        obj.SetActive(true);
        currentMenu = obj;
    }

    public void unPauseGame(GameObject obj)
    {
        obj.SetActive(false);
    }

    public void pauseGame(GameObject obj)
    {
        //Pause timer
        currentMenu = obj;
        obj.SetActive(true);
    }

    public void quitGame()
    {
        Application.Quit();
    }

    public void endGame(GameObject obj)
    {
        //disable current menu, usually pause at this point.
        currentMenu.SetActive(false);
        //disable game UI
        
        //show game over screen
        obj.SetActive(true);
    }


}
