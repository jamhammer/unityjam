﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuSystem : Menu {

    public Text gameTimerText;
    public float gameLength;


    //public void loadLevel(string level)
    //{
    //    currentMenu.SetActive(false);
    //    if (level == "Mode1")
    //    {
    //        GameUI.SetActive(true);
    //        //gameTimerText = FindObjectsOfType<Text>();
    //        gameTime();
    //    }
    //    if (level == "Mode2")
    //    {
    //        GameUI.SetActive(true);
    //    }

    //}

    public void gameTime()
    {
        int minutes = Mathf.FloorToInt(gameLength / 60f);
        int seconds = Mathf.FloorToInt(gameLength - minutes * 60);
        string gameTimeFormat = string.Format("{0:0}:{1:00}", minutes, seconds);
        gameTimerText.text = gameTimeFormat;
    }
}
