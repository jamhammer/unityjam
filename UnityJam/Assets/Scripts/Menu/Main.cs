﻿using UnityEngine;
using System.Collections;

public class Main : Menu {

    public GameObject startMenu;
    
    void Start()
    {
        currentMenu = startMenu;
        startMenu.SetActive(true);
    }
}
