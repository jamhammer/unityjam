﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Hud : MonoBehaviour {

    GameController gc;
    public GameObject modeUI;
    public Text countdownText, p1ScoreTxt, p2ScoreTxt;

    void Start()
    {
        gc = GetComponent<GameController>();
        p1ScoreTxt.text = gc.p1Score.ToString();
        p2ScoreTxt.text = gc.p2Score.ToString();
    }




}
